﻿using System;
using System.Web;
using System.Web.Mvc;
using StructureMap;

namespace CanisMajoris.Web.Mvc.StructureMap
{
    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        private readonly IContainer _container;

        public StructureMapControllerFactory(IContainer container)
        {
            _container = container;
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404, string.Format("Invalid contorller: {0} ", requestContext.HttpContext.Request.Path));
            }
            else
                return (IController)_container .GetInstance(controllerType);

        }
    }
}