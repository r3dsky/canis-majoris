﻿using System;
using System.Data;
using System.Data.SqlClient;
using CanisMajoris.Framework.Configuration.Database.Component;
using CanisMajoris.Framework.Configuration.Database.Interfaces;
using CanisMajoris.Framework.Configuration.Database.Models;
using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.Framework.Repositories.Specified;
using Dapper;

namespace CanisMajoris.Framework.Configuration.Database
{
    public class TableConfigurator : SqlRepository, ITableConfigurator
    {

        public TableConfigurator(IConnectionStringResolver connectionStringResolver):base(connectionStringResolver)
        {
        }

       

        public CreateTableResult CreateTable(Type tableType)
        {
            CreateTableResult tableCreated = CreateTableResult.Undefined;
 //           if (!TableExists(tableType.Name))
   //         {
                Do(connection =>
                {
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = FormatIfNotExistsCreateTable(FormatIfNotTableExistsString(tableType.Name), SqlTableScriptFactory.Create(tableType));
                    cmd.CommandType = CommandType.Text;
                    var results = (int)cmd.ExecuteNonQuery();
                    // z resultem cos jest nie teges, nie zwraca created
                    tableCreated = results > 0 ? CreateTableResult.Created : CreateTableResult.Failed;
                    //poki co ustawiam zawsze true
                    tableCreated = CreateTableResult.Created;
                });
            //}
            //else
            //{
            //    tableCreated = CreateTableResult.ExistsAready;
            //}
            return tableCreated;
        }

        private string FormatIfNotExistsCreateTable(string ifTableNotExistsSql, string createTableSql)
        {
            return string.Concat(ifTableNotExistsSql, " BEGIN ", createTableSql, " END");
        }
      
        private string FormatIfNotTableExistsString(string tableName)
        {

            string tpl = "IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = '{0}'))";
            return string.Format(tpl, tableName);
        }

    }
}