﻿using System.Runtime.InteropServices;
using CanisMajoris.Framework.Configuration.Database.Component;
using Dapper;

namespace CanisMajoris.Framework.Configuration.Application
{
    public interface IApplicationInitializer
    {
        void Initialize();
    }
}