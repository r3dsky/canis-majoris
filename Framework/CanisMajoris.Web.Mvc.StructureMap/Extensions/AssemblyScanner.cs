﻿using System.Collections.Generic;
using System.Reflection;
using StructureMap;

namespace CanisMajoris.Web.Mvc.StructureMap.Extensions
{
    public static class AssemblyScanner
    {
        public static void ScanAssembliesWithDefaultConvention(this ConfigurationExpression cfg,
            IEnumerable<Assembly> assemblies)
        {
            cfg.Scan(s =>
            {
                foreach (var canisMajorisAssembly in assemblies)
                {
                    s.Assembly(canisMajorisAssembly);
                }
                s.WithDefaultConventions();
            });
        }
    }
}