﻿namespace CanisMajoris.Framework.Repositories.Models
{
    public class PageSettings
    {
        public int PageSize { get; set; }
        public int PageNo { get; set; }

    }
}