﻿using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.UserManagement.Domain.Models;

namespace CanisMajoris.UserManagement.Domain.Contracts.Repositories
{
    public interface IUserRepository :IGenericRepository<User,UserSearchCriteria>
    {
    }
}