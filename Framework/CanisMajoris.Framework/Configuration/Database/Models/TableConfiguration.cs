﻿using CanisMajoris.Framework.Configuration.Database.Attributes;

namespace CanisMajoris.Framework.Configuration.Database.Models
{
    public class TableConfiguration
    {
        [SqlFieldPrimaryKey]
        [SqlFieldIdentitySpecification(true)]
        public int Id { get; set; }
        public string Table { get; set; }
        public int Verions { get; set; }
    }
}