﻿namespace CanisMajoris.Framework.Repositories.Interfaces
{
    public interface IConnectionStringResolver
    {
        string GetByAssemblyName(string moduleName);
    }
}