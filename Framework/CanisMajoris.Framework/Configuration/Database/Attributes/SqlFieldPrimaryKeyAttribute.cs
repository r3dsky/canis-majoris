﻿using System;

namespace CanisMajoris.Framework.Configuration.Database.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SqlFieldPrimaryKeyAttribute : Attribute
    {
         
    }
}