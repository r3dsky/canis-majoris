﻿using System.Collections.Generic;
using System.Linq;
using CanisMajoris.Framework.Repositories.Interfaces;

namespace CanisMajoris.Framework.Repositories.Models
{
    public class SearchResults<TModel> : ISearchResults<TModel> where TModel: class
    {
        public IEnumerable<TModel> Items { get; set; }
        public PageSettings Pager { get; set; }
        public int Total { get; set; }

        public SearchResults()
        {
            Items = Enumerable.Empty<TModel>();
            Pager = new PageSettings();
        }
    }
}