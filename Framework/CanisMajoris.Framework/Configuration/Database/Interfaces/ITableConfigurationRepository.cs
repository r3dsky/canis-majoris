﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using CanisMajoris.Framework.Configuration.Database.Models;

namespace CanisMajoris.Framework.Configuration.Database.Interfaces
{
    public interface ITableConfigurationRepository
    {
        IEnumerable<TableConfiguration> GetConfigurations();
        TableConfiguration GetTableConfigurations(string tableName);
        bool UpdateTableConfiguration(TableConfiguration table);
        bool AddTableConfiguration(TableConfiguration table);

    }
}