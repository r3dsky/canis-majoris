﻿using CanisMajoris.Framework.Repositories.Models;

namespace CanisMajoris.Framework.Repositories.Interfaces
{
    public interface ISearchCriteria
    {
        PageSettings Pager { get; set; }
    }
}