﻿using System.Collections.Generic;
using CanisMajoris.Framework.Repositories.Models;

namespace CanisMajoris.Framework.Repositories.Interfaces
{
    public interface ISearchResults<TModel> where TModel : class
    {
        IEnumerable<TModel> Items { get; set; }
        PageSettings Pager { get; set; }
        int Total { get; set; }
    }
}