﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CanisMajoris.Framework.Configuration.Modules
{
    public static class ModulesOrderResolver
    {

        public static IEnumerable<IModuleInitializer> OrderByDependency(IEnumerable<IModuleInitializer> initializers)
        {
            var orderedModuleInitializers= new List<IModuleInitializer>();

            var initalizersDict = initializers.ToDictionary(
                x => x, 
                x => x.GetType().GetCustomAttributes(typeof (DependensOnAttribute)).Cast<DependensOnAttribute>().ToList());
            
            orderedModuleInitializers.AddRange(initalizersDict.Where(x => !x.Value.Any()).Select(x => x.Key));
            
            var hasChange = false;
            var dependentModules = initalizersDict.Where(x=>x.Value.Any()).Select(x => x.Key).ToList();
            var dependentCount = dependentModules.Count;
            var dependentModulesDict = new Dictionary<IModuleInitializer, List<DependensOnAttribute>>(); 
            do
            {
                hasChange = false;
                dependentModulesDict = dependentModules.ToDictionary(x => x, k => initalizersDict.FirstOrDefault(v => v.Key.Equals(k)).Value);
                dependentModules = dependentModulesDict.Select(x => x.Key).ToList();
                var i = 0;
                while (i < dependentCount && !hasChange)
                {
                    var kvp = dependentModulesDict.ElementAt(i);
                    var moduleDependenciesTypes = kvp.Value.SelectMany(x => x.IModuleInitializerTypes);
                    var parents = dependentModulesDict.Where(
                        x => moduleDependenciesTypes.Any(m => m.IsAssignableFrom(x.Key.GetType())));

                    foreach (var parent in parents)
                    {
                        if (!parent.IsDefault())
                        {
                            var parentIndex = dependentModules.IndexOf(parent.Key);
                            if (i < parentIndex)
                            {
                                var childIndex = dependentModules.IndexOf(kvp.Key);
                                dependentModules.RemoveAt(childIndex);
                                dependentModules.Insert(parentIndex,kvp.Key);
                                hasChange = true;
                            }
                        }
                    }

                    i++;
                }
            } while (hasChange); 
            
            orderedModuleInitializers.AddRange(dependentModules);

            return orderedModuleInitializers;
        }

        public static bool IsDefault<T>(this T value) where T : struct
        {
            bool isDefault = value.Equals(default(T));

            return isDefault;
        }
    }
}
