﻿using System;
using System.Data;
using System.Linq;

namespace CanisMajoris.Framework.Configuration.Modules
{
    public class DependensOnAttribute:Attribute
    {
        private readonly Type[] _iModuleInitializers;


        public Type[] IModuleInitializerTypes
        {
            get { return _iModuleInitializers; }
        }

        public DependensOnAttribute(params Type[] iModuleInitializers)
        {
            if (iModuleInitializers==null )
                throw new NoNullAllowedException();

            if (!iModuleInitializers.Any())
                throw new ArgumentException("non empty required");
            _iModuleInitializers = iModuleInitializers;
        }

    }
}