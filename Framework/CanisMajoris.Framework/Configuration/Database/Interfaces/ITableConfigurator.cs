﻿using System;
using CanisMajoris.Framework.Configuration.Database.Models;

namespace CanisMajoris.Framework.Configuration.Database.Interfaces
{
    public interface ITableConfigurator
    {
        /// <summary>
        /// creates table if it not exists, invokes TableExists internaly
        /// </summary>
        /// <param name="tableType"></param>
        /// <returns>true if table is created or already exists</returns>
        CreateTableResult CreateTable(Type tableType);
    }
}