﻿using System;

namespace CanisMajoris.Framework.Configuration.Database.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SqlFieldIsNullableAttribute : Attribute
    {
        private readonly bool _isNullable;

        public bool IsNullable
        {
            get { return _isNullable; }
        }

        public SqlFieldIsNullableAttribute(bool isNullable)
        {
            _isNullable = isNullable;
        }

    }
}