using System.Collections.Generic;
using System.Data.SqlClient;
using CanisMajoris.Framework.Configuration.Database.Interfaces;
using CanisMajoris.Framework.Configuration.Database.Models;
using CanisMajoris.Framework.Configuration.Modules;
using CanisMajoris.Framework.Repositories.Interfaces;

namespace CanisMajoris.Framework.Configuration.Application
{
    public class ApplicationInitializer : IApplicationInitializer
    {
        private readonly IEnumerable<IModuleInitializer> _moduleInitializers;
        private readonly ITableConfigurator _tableConfigurator;
        
        public ApplicationInitializer(IEnumerable<IModuleInitializer> moduleInitializers, ITableConfigurator tableConfigurator)
        {
            _moduleInitializers = moduleInitializers;
            _tableConfigurator = tableConfigurator;
        }

        public void Initialize()
        {
            _tableConfigurator.CreateTable(typeof (TableConfiguration));

            var orderedModules = ModulesOrderResolver.OrderByDependency(_moduleInitializers);
            foreach (var moduleInitializer in orderedModules)
            {
                moduleInitializer.Initialize();
            } 

        }
    }
}