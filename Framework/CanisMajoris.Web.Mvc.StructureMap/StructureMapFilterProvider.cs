﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StructureMap;

namespace CanisMajoris.Web.Mvc.StructureMap
{
    public class StructureMapFilterProvider : FilterAttributeFilterProvider
    {
        private readonly IContainer _container;

        public StructureMapFilterProvider(IContainer container)
        {
            _container = container;
        }

        public override IEnumerable<Filter> GetFilters(ControllerContext controllerContext,
                  ActionDescriptor actionDescriptor)
        {

            var filters = base.GetFilters(controllerContext, actionDescriptor);

            if (filters != null)
            {
                foreach (var filter in filters)
                {
                    var injectableProps =
                        filter.Instance.GetType()
                            .GetProperties()
                            .Where(
                                x => x.PropertyType.IsInterface &&
                                _container.Model.AllInstances.Any(i => i.PluginType == x.PropertyType));

                    foreach (var injectableProp in injectableProps)
                    {
                        injectableProp.SetValue(filter.Instance, _container.GetInstance(injectableProp.PropertyType));
                    }
                }

                return filters;
            }

            return default(IEnumerable<Filter>);
        }
    }

}