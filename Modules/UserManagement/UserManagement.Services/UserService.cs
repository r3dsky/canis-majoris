﻿using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.UserManagement.Domain.Contracts.Repositories;
using CanisMajoris.UserManagement.Domain.Contracts.Services;
using CanisMajoris.UserManagement.Domain.Models;

namespace CanisMajoris.UserManagement.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public int AddUser(User user)
        {
            return _userRepository.Add(user);
        }

        public bool UpdateUser(User user)
        {
            return _userRepository.Update(user);
        }

        public bool DeleteUser(User user)
        {
            return _userRepository.Delete(user);
        }

        public ISearchResults<User> Search(UserSearchCriteria searchCriteria)
        {
            return _userRepository.Search(searchCriteria);
        }
    }
}