﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CanisMajoris.Framework.Configuration.Database.Attributes;

namespace CanisMajoris.Framework.Configuration.Database.Component
{
    public static class SqlTableScriptFactory
    {

        public static string Create(Type type)
        {
            var props = type.GetProperties();
            
            var sqlFields = new List<string>();
            var pkProps = new List<string>();
            foreach (var propertyInfo in props)
            {
                var sqlFieldName = ToSqlFieldName(propertyInfo);
                var sqlTypeName = ToSqlType(propertyInfo);
                var identityDeclaration = ToIdentityDeclaration(propertyInfo);
                var nullDeclaration = ToNullDeclaration(IsPropertyNullable(propertyInfo));

                if (propertyInfo.GetCustomAttribute<SqlFieldPrimaryKeyAttribute>()!=null)
                    pkProps.Add(sqlFieldName);

                sqlFields.Add(string.Join(" ", new[] { sqlFieldName, sqlTypeName, identityDeclaration, nullDeclaration }));
            }

            string createTableScript = CreateTableScript(type.Name, sqlFields);
            string alterTableScript = AlterTableScript(type.Name, pkProps);
            

            return string.Join(" ",new []{createTableScript,alterTableScript});
        }

        private static string AlterTableScript(string tableName, List<string> pkProps)
        {
            string alterTableTpl = string.Format("ALTER TABLE dbo.{0}", tableName);

            pkProps = pkProps.Select(x => x.Replace("[", string.Empty).Replace("]", string.Empty)).ToList();

            var alterPieces = new List<string>();
            if (pkProps.Any())
            {
                var constraintScript = pkProps.Count > 1 ? string.Format("CONSTRAINT pk_{1}", tableName) : string.Empty;

                alterPieces.Add(string.Format("{0} ADD {1} PRIMARY KEY ({2})", alterTableTpl, constraintScript,string.Join(",",pkProps)));
            }
            return string.Join(" ", alterPieces);
        }

        private static string CreateTableScript(string tableName, IEnumerable<string> sqlFields)
        {
            var createTalbePieces = new[]
            {
                "CREATE TABLE",
                string.Format("dbo.{0}",tableName),
                "(",
                string.Join(", ",sqlFields),
                ")"
            };
            var createTableScript = string.Join(" ", createTalbePieces);
            return createTableScript;
        }


        private static string ToSqlFieldName(PropertyInfo propertyInfo)
        {
            return string.Format("[{0}]", propertyInfo.Name);
        }

        private static string ToIdentityDeclaration(PropertyInfo propertyInfo)
        {

            var attribs = propertyInfo.GetCustomAttributes();
            var pkAttribute =
                propertyInfo.GetCustomAttributes(typeof (SqlFieldIdentitySpecificationAttribute))
                    .Cast<SqlFieldIdentitySpecificationAttribute>()
                    .FirstOrDefault();
            if (pkAttribute == null)
                return string.Empty;

            if (pkAttribute.Seed.HasValue)
                return string.Format("IDENTITY ({0},{1})", pkAttribute.IsIncremental? 1 : 0, pkAttribute.Seed.Value);
            else
                return string.Format("IDENTITY ({0})", pkAttribute.IsIncremental);
        }

        private static string ToSqlType(PropertyInfo propertyInfo)
        {
            const string nvarcharName = "system.string";
            const string intName = "system.int32";
            const string bitName = "system.boolean";
            const string datetimeName= "system.datetime";

            var propName = propertyInfo.PropertyType.FullName.ToLower();
            switch (propName)
            {
                case nvarcharName :
                    var stringLengthAttribute =
                        propertyInfo.GetCustomAttributes(typeof (SqlFieldNVarCharLengthAttribute))
                            .Cast<SqlFieldNVarCharLengthAttribute>()
                            .FirstOrDefault();
                    var stringLength = "max";
                    if (stringLengthAttribute != null)
                    {
                        stringLength = stringLengthAttribute.IsMax ? "max" : stringLength.Length.ToString();
                    }
                    return string.Format("[{0}]({1})", "nvarchar",stringLength);
                case intName :
                    return "[int]";
                case bitName:
                    return "[bit]";
                case datetimeName:
                    return "[datetime]";
                default:
                    throw new Exception(string.Format("invalid type: {0}", propName));
            }
        }

        private static string ToNullDeclaration(bool isNullable)
        {
            
            return isNullable ? "NULL" : "NOT NULL";

        }

        private static bool IsPropertyNullable(PropertyInfo propertyInfo)
        {
            var isNullableAttribute = propertyInfo.GetCustomAttributes(typeof(SqlFieldIsNullableAttribute)).Cast<SqlFieldIsNullableAttribute>().FirstOrDefault();
            if (isNullableAttribute != null)
                return isNullableAttribute.IsNullable;

            var type = propertyInfo.PropertyType;
            return (type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>));
        }

    }
}
