﻿using System;

namespace CanisMajoris.Framework.Configuration.Database.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SqlFieldNVarCharLengthAttribute : Attribute
    {
        private readonly bool _isMax;
        private readonly int _length;

        public bool IsMax
        {
            get { return _isMax; }
        }

        public int Length
        {
            get { return _length; }
        }


        public SqlFieldNVarCharLengthAttribute(bool isMax)
        {
            _isMax = isMax;
        }

        public SqlFieldNVarCharLengthAttribute(int length)
        {
            _length = length;
        }
    }
}