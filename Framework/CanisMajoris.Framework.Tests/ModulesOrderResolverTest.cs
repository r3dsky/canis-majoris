﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanisMajoris.Framework.Configuration.Modules;
using NUnit.Framework;

namespace CanisMajoris.Framework.Tests
{


    public abstract class ParentClass : IModuleInitializer
    {
        public void Initialize()
        {
            throw new NotImplementedException();
        }
    }

    public class ParentA : ParentClass, IModuleInitializer
    {}

    public class ParentB : ParentClass, IModuleInitializer
    {}

    [DependensOn(typeof(ParentA))]
    public class ParentC : ParentClass, IModuleInitializer
    { }

    [DependensOn(typeof(ParentA))]
    public class ParentAA : ParentClass, IModuleInitializer
    {}

    [DependensOn(typeof(ParentA))]
    public class ParentAB : ParentClass, IModuleInitializer
    {}

    [DependensOn(typeof(ParentAA))]
    public class ParentAAB : ParentClass, IModuleInitializer
    {}

    [DependensOn(typeof(ParentAAB),typeof(ParentC))]
    public class ParentCAAB : ParentClass, IModuleInitializer
    { }

    public class ModulesOrderResolverTest
    {
        [Test]
        public void Test()
        {
            //arrange
            var parentA = new ParentA();
            var parentB = new ParentB();
            var parentC = new ParentC();
            var parentAA = new ParentAA();
            var parentAB = new ParentAB();
            var parentAAB = new ParentAAB();
            var parentCAAB = new ParentCAAB();

            var moduleInitializers = new List<IModuleInitializer> { parentCAAB, parentAAB, parentAB, parentC, parentB, parentA, parentAA };

            //act
            var orderedModules = ModulesOrderResolver.OrderByDependency(moduleInitializers).ToList();
            
            //assert
            var indexOfA = orderedModules.IndexOf(parentA);
            var indexOfAA = orderedModules.IndexOf(parentAA);
            var indexOfAAB = orderedModules.IndexOf(parentAAB);
            var indexOfAB = orderedModules.IndexOf(parentAB);
            var indexOfCAAB = orderedModules.IndexOf(parentCAAB);
            var indexOfC= orderedModules.IndexOf(parentC);
  

            Assert.IsTrue(indexOfA < indexOfAA, "indexOfA < indexOfAA fail");
            Assert.IsTrue(indexOfA < indexOfAAB, "indexOfA < indexOfAAB fail");
            Assert.IsTrue(indexOfA < indexOfC, "indexOfA < indexOfC fail");
            Assert.IsTrue(indexOfA < indexOfAB, " indexOfA < indexOfAB fail");
            Assert.IsTrue(indexOfAA < indexOfAAB, "indexOfAA < indexOfAAB fail");
            Assert.IsTrue(indexOfAAB < indexOfCAAB, "indexOfAAB < indexOfCAAB fail");
            Assert.IsTrue(indexOfC < indexOfCAAB, "indexOfC < indexOfCAAB fail");


        }
    }
}
