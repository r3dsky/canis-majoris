﻿using System.Collections.Generic;
using System.Linq;
using CanisMajoris.Framework.Configuration.Database.Interfaces;
using CanisMajoris.Framework.Configuration.Database.Models;
using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.Framework.Repositories.Specified;
using DapperExtensions;

namespace CanisMajoris.Framework.Configuration.Database.Repositories
{
    public class TableConfigurationRepository : SqlRepository, ITableConfigurationRepository
    {
        public TableConfigurationRepository(IConnectionStringResolver connectionStringResolver) : base(connectionStringResolver)
        {
        }

        public IEnumerable<TableConfiguration> GetConfigurations()
        {
            IEnumerable<TableConfiguration>  results = null; 
            Do(cn =>
            {
                results = cn.GetList<TableConfiguration>();
            });
            return results ?? Enumerable.Empty<TableConfiguration>();
        }

        public TableConfiguration GetTableConfigurations(string tableName)
        {
            TableConfiguration table = null;
            Do(cn =>
            {
                var predicate = Predicates.Field<TableConfiguration>(f => f.Table, Operator.Eq, tableName);
                table = cn.GetList<TableConfiguration>(predicate).FirstOrDefault();
            });
            return table;
        }

        public bool UpdateTableConfiguration(TableConfiguration table)
        {
            bool updated = false;
            Do(cn =>
            {
                updated =  cn.Update(table);

            });
            return updated;
        }

        public bool AddTableConfiguration(TableConfiguration table)
        {
            bool created = false;
            Do(cn =>
            {
                created  = cn.Insert(table) >0;
            });
            return created;
        }
    }
}