﻿using System;

namespace CanisMajoris.Framework.Configuration.Database.Attributes
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple = false)]
    public class SqlFieldIdentitySpecificationAttribute:Attribute
    {
        private readonly bool _isIncremental;
        private readonly int? _seed;

        public SqlFieldIdentitySpecificationAttribute(bool isIncremental, int seed)
        {
            _isIncremental = isIncremental;
            _seed = seed;
        }

        public SqlFieldIdentitySpecificationAttribute(bool isIncremental)
        {
            _isIncremental = isIncremental;
            _seed = 1;
        }

        public bool IsIncremental
        {
            get { return _isIncremental; }
        }

        public int? Seed
        {
            get { return _seed; }
        }
    }
}