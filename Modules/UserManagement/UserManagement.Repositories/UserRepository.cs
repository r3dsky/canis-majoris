﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.Framework.Repositories.Models;
using CanisMajoris.Framework.Repositories.Specified;
using CanisMajoris.UserManagement.Domain.Contracts.Repositories;
using CanisMajoris.UserManagement.Domain.Models;
using CanisMajoris.UserManagement.Repositories.Models;
using DapperExtensions;
using UserDomain = CanisMajoris.UserManagement.Domain.Models.User;

namespace CanisMajoris.UserManagement.Repositories
{
    public class UserRepository : SqlRepository, IUserRepository
    {
     
        public UserRepository(IConnectionStringResolver connectionStringResolver)
            :base(connectionStringResolver)
        {
        }
        
        public int Add(UserDomain user)
        {
            int userId = 0;
            Do(connection => userId = connection.Insert(ToDbUser(user)));
            return userId;
        }

        public bool Update(UserDomain user)
        {
            var isSuccess = false;
            var updatedUser = Search(new UserSearchCriteria() { Id = user.Id }).Items.FirstOrDefault();
            if (updatedUser == null)
                return isSuccess;

            CopyValues(user, updatedUser);
            var dbUser = ToDbUser(updatedUser);
            
            Do(connection =>
            {
                isSuccess = connection.Update(dbUser);
            });
            return isSuccess;
        }

        public bool Delete(UserDomain user)
        {
            var isSuccess = false;
            Do(connection =>
            {
                var predicate = Predicates.Field<Users>(f => f.Id, Operator.Eq, user.Id);
                isSuccess = connection.Delete(predicate);

            });
            return isSuccess;
        }
        
        public ISearchResults<UserDomain> Search(UserSearchCriteria searchCriteria)
        {
            var results = new SearchResults<User>();
            var predicate = Predicates.Field<Users>(f => f.Id, Operator.Eq, searchCriteria.Id);

            var sort = new List<ISort>();
            sort.Add(new Sort(){PropertyName = "Id",Ascending = true });


            Do(connection =>
            {
                results.Items = connection.GetPage<Users>(predicate, sort, searchCriteria.Pager.PageNo, searchCriteria.Pager.PageSize)
                    .Select(ToDomainUser);
                results.Total = connection.Count<Users>(predicate);
            });

            return results;
        }

   
        #region converters

        private UserDomain ToDomainUser(Users dbUser)
        {
            return new UserDomain()
            {
                BlockReason = dbUser.BlockReason,
                Created = dbUser.Created,
                Email = dbUser.Email,
                Id = dbUser.Id,
                IsActive = dbUser.IsActive,
                IsBlocked = dbUser.IsBlocked,
                Password = dbUser.Password,
                Updated = dbUser.Updated
            };
        }

        private Users ToDbUser(UserDomain domainUser, Users dbUser = null)
        {
            dbUser = dbUser ?? new Users();
            dbUser.BlockReason = domainUser.BlockReason;
            dbUser.Created = domainUser.Created;
            dbUser.Email = domainUser.Email;
            dbUser.Id = domainUser.Id;
            dbUser.IsActive = domainUser.IsActive;
            dbUser.IsBlocked = domainUser.IsBlocked;
            dbUser.Password = domainUser.Password;
            dbUser.Updated = domainUser.Updated;
            return dbUser;
        }
        
        #endregion
    }
}