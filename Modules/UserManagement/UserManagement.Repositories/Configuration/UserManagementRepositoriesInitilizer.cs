﻿using System.Data;
using System.Data.SqlClient;
using CanisMajoris.Framework.Configuration.Database.Component;
using CanisMajoris.Framework.Configuration.Database.Interfaces;
using CanisMajoris.Framework.Configuration.Database.Models;
using CanisMajoris.Framework.Configuration.Modules;
using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.UserManagement.Repositories.Models;
using Dapper;

namespace CanisMajoris.UserManagement.Repositories.Configuration
{
    public class UserManagementRepositoriesInitilizer : IModuleInitializer
    {
        private readonly ITableConfigurator _tableConfigurator;
        private readonly ITableConfigurationRepository _tableConfigurationRepository;

        public UserManagementRepositoriesInitilizer(ITableConfigurator tableConfigurator,ITableConfigurationRepository tableConfigurationRepository)
        {
            _tableConfigurator = tableConfigurator;
            _tableConfigurationRepository = tableConfigurationRepository;
        }

        public void Initialize()
        {
            if (_tableConfigurator.CreateTable(typeof (Users)) == CreateTableResult.Created)
            {

                _tableConfigurationRepository.AddTableConfiguration(new TableConfiguration()
                {
                    Table = typeof(Users).FullName,
                    Verions = 1
                });
            }
        }
    }
}