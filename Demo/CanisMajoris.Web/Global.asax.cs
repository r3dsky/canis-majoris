﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using CanisMajoris.Framework.Configuration.Application;
using CanisMajoris.Framework.Configuration.Modules;
using CanisMajoris.UserManagement.Domain.Contracts.Services;
using CanisMajoris.Web.Mvc.StructureMap;
using CanisMajoris.Web.Mvc.StructureMap.Extensions;

namespace CanisMajoris.Demo.Web
{

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var canisMajorisAssemblies = GetAssembliesToScan();
            var container = StructureMapInitializer.Initialize(assemblies: canisMajorisAssemblies);
            
            container.GetInstance<IApplicationInitializer>().Initialize();
      
        }

        private ICollection<Assembly> GetAssembliesToScan()
        {
            var canisPrefix = typeof(IUserService).Assembly.FullName.Split('.')[0];
            var canisMajorisAssemblies =
                AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.StartsWith(canisPrefix)).ToList();
            return canisMajorisAssemblies;
        }
    }
}