﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CanisMajoris.Framework.Configuration.Modules;
using StructureMap;

namespace CanisMajoris.Web.Mvc.StructureMap.Extensions
{
    public static class ModulesIntializersConfigurator
    {
        public static void SetupModulesInitializersByAssemblies(this ConfigurationExpression cfg,  ICollection<Assembly> assemblies)
        {
            cfg.ScanAssembliesWithDefaultConvention(assemblies);
            var moduleInitializersTypes = assemblies.SelectMany(x => x.GetTypes())
                    .Where(x => typeof(IModuleInitializer).IsAssignableFrom(x) && typeof(IModuleInitializer) != x);
            foreach (var moduleInitializerType in moduleInitializersTypes)
            {
                cfg.For(typeof(IModuleInitializer)).Use(moduleInitializerType);
            }
        }
    }
}