﻿using System;
using System.Data;
using System.Data.SqlClient;
using CanisMajoris.Framework.Repositories.Interfaces;

namespace CanisMajoris.Framework.Repositories.Specified
{
    public abstract class SqlRepository
    {
        private readonly IConnectionStringResolver _connectionStringResolver;

        protected SqlRepository(IConnectionStringResolver connectionStringResolver)
        {
            _connectionStringResolver = connectionStringResolver;
        }

        protected void Do(Action<IDbConnection> action)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                action(connection);
            }
        }

        protected IDbConnection GetConnection()
        {
            var sqlConnection = new SqlConnection(_connectionStringResolver.GetByAssemblyName(this.GetType().Assembly.FullName));
            return sqlConnection;
        }

        protected void CopyValues<TModel>(TModel source, TModel dest)
        {
            foreach (var prop in typeof(TModel).GetProperties())
            {
                prop.SetValue(dest, prop.GetValue(source));
            }
        }
   
    }
}