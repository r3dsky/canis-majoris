﻿using System;
using CanisMajoris.Framework.Configuration.Database.Attributes;

namespace CanisMajoris.UserManagement.Repositories.Models
{
    public class Users
    {
        [SqlFieldPrimaryKey]
        [SqlFieldIdentitySpecification(true)]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlocked { get; set; }
        public string BlockReason { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    } 
}