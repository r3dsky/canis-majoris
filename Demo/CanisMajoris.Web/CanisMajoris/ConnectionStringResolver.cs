﻿using System.Web.Configuration;
using CanisMajoris.Framework.Repositories.Interfaces;

namespace CanisMajoris.Demo.Web.CanisMajoris
{
    public class ConnectionStringResolver : IConnectionStringResolver
    {
        public string GetByAssemblyName(string moduleName)
        {
            return GetMainConnectionString();
        }

        public string GetMainConnectionString()
        {
            return WebConfigurationManager.ConnectionStrings["CanisMajorisWeb"].ConnectionString;
        }
    }
}