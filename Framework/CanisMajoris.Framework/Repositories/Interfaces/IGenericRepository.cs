﻿namespace CanisMajoris.Framework.Repositories.Interfaces
{
    public interface IGenericRepository<TModel, in TSearchCriteria>
         where TModel : class
         where TSearchCriteria: class, ISearchCriteria
    {
        int Add(TModel user);
        bool Update(TModel user);
        bool Delete(TModel user);
        ISearchResults<TModel> Search(TSearchCriteria searchCriteria);
    }
}