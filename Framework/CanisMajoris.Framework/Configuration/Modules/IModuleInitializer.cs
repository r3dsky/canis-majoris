﻿namespace CanisMajoris.Framework.Configuration.Modules
{
    public interface IModuleInitializer
    {
        void Initialize();
    }
}