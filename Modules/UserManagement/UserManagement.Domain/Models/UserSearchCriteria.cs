﻿using CanisMajoris.Framework.Repositories.Models;

namespace CanisMajoris.UserManagement.Domain.Models
{
    public class UserSearchCriteria:SearchCriteria
    {
        public int Id { get; set; }
    }
}