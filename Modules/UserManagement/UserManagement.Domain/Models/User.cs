﻿using System;

namespace CanisMajoris.UserManagement.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlocked { get; set; }
        public string BlockReason { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }

}