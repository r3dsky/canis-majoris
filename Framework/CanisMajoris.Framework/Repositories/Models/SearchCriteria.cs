﻿using CanisMajoris.Framework.Repositories.Interfaces;

namespace CanisMajoris.Framework.Repositories.Models
{
    public abstract class SearchCriteria : ISearchCriteria
    {
        public PageSettings Pager { get; set; }

        protected SearchCriteria()
        {
            Pager = new PageSettings();
        }
    }
}