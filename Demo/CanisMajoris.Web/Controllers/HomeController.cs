﻿using System.Web.Mvc;
using CanisMajoris.UserManagement.Domain.Contracts.Services;
using CanisMajoris.UserManagement.Domain.Models;

namespace CanisMajoris.Demo.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            var results= _userService.Search(new UserSearchCriteria()).Items;

            return View();
        }

    }
}
