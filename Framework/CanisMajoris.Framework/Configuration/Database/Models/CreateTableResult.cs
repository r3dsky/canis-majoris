﻿namespace CanisMajoris.Framework.Configuration.Database.Models
{
    public enum CreateTableResult
    {
        Undefined,
        Created,
        Failed,
        ExistsAready
    }
}