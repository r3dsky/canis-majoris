﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using CanisMajoris.Web.Mvc.StructureMap.Extensions;
using StructureMap;
using StructureMap.Graph;

namespace CanisMajoris.Web.Mvc.StructureMap
{
    public class StructureMapInitializer
    {


        public static IContainer Initialize(Action<ConfigurationExpression> additionalConfiguration = null, ICollection<Assembly> assemblies = null)
        {
            additionalConfiguration = additionalConfiguration ?? new Action<ConfigurationExpression>(x => { });
            assemblies = assemblies ?? new List<Assembly>();

#pragma warning disable 0618
            ObjectFactory.Configure(cfg =>
            {
                cfg.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });


                additionalConfiguration(cfg);

                cfg.SetupModulesInitializersByAssemblies(assemblies);
            });

            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory(ObjectFactory.Container));
            var oldProvider = FilterProviders.Providers.Single(f => f is FilterAttributeFilterProvider);
            FilterProviders.Providers.Remove(oldProvider);
            FilterProviders.Providers.Add(new StructureMapFilterProvider(ObjectFactory.Container));

            return ObjectFactory.Container;

#pragma warning restore 0618
        }
    }
}