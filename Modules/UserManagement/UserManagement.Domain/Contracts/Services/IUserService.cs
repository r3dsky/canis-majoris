﻿using CanisMajoris.Framework.Repositories.Interfaces;
using CanisMajoris.UserManagement.Domain.Models;

namespace CanisMajoris.UserManagement.Domain.Contracts.Services
{
    public interface IUserService
    {
        int AddUser(User user);
        bool UpdateUser(User user);
        bool DeleteUser(User user);
        ISearchResults<User> Search(UserSearchCriteria searchCriteria);
    }
}